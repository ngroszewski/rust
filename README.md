# rust

Install and manage rust

## Dependencies

* [polkhan.asdf](https://gitlab.com/polkhan/asdf.git)
  _The asdf configs must be included in the users `playbook.yml` since no
  default configs are passed to the dependent role_

## Role Variables

* `versions`:
    * Type: List
    * Usages: List of Rust versions to install

* `global_version`:
    * Type: String
    * Usages: Rust version to make global default

```
rust:
  versions:
    - 1.31.0
  global_version: 1.31.0
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polkhan.rust

## License

MIT
